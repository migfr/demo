/*
 * WeldingData.h
 *
 *  Created on: 15 avr. 2021
 *      Author: mig
 */

#ifndef SOMEDATA_HPP_
#define SOMEDATA_HPP_

#include "InterprocessData.hpp"

class SomeData: public InterprocessData {
	int weld1 = 0;
	int weld2 = 0;
public:
	SomeData() = default;

	int getWeld1() const;

	int getWeld2() const;

	void setWeld1(int value) {
		this->weld1 = value;
	}
	void setWeld2(int value) {
		this->weld2 = value;
	}
};

#endif /* SOMEDATA_HPP_ */
