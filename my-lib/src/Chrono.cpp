/*
 * Chrono.cpp
 *
 *  Created on: 17 avr. 2021
 *      Author: mig
 */

#include "Chrono.hpp"

std::chrono::milliseconds Chrono::getElapsed() const {
	return std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::high_resolution_clock::now() - start);
}

void Chrono::reset() {
	start = std::chrono::high_resolution_clock::now();
}
