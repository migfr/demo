/*
 * InterprocessData.h
 *
 *  Created on: 15 avr. 2021
 *      Author: mig
 */

#ifndef INTERPROCESSDATA_HPP_
#define INTERPROCESSDATA_HPP_

#include "../boost_1_75_0/boost/interprocess/sync/interprocess_mutex.hpp"

class InterprocessData: public boost::interprocess::interprocess_mutex {
public:
	using interprocess_mutex::interprocess_mutex;
	InterprocessData() = default;
	virtual ~InterprocessData() = default;
	InterprocessData(const InterprocessData &other) = default;
};

#endif /* INTERPROCESSDATA_HPP_ */
