/*
 * IPCSharedMem.h
 *
 *  Created on: 16 avr. 2021
 *      Author: mig
 */

#ifndef IPCSHAREDMEM_H_
#define IPCSHAREDMEM_H_

#include <boost/interprocess/detail/os_file_functions.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/creation_tags.hpp>
#include <stddef.h>
#include <string>

class IPCSharedMem: public boost::interprocess::shared_memory_object {

public:
	IPCSharedMem() = delete;
	// Creates a shared memory of size "size". OVERWRITEs if already exists
	explicit IPCSharedMem(boost::interprocess::open_or_create_t,
			const std::string &name, boost::interprocess::mode_t mode,
			size_t size) :
			boost::interprocess::shared_memory_object(
					boost::interprocess::open_or_create, name.c_str(), mode), name(
					name), automaticDelete(name.c_str()) {
		this->truncate(size);
	}
	// Opens a shared memory named "name". Throws if not already exists
	explicit IPCSharedMem(boost::interprocess::open_only_t,
			const std::string &name, boost::interprocess::mode_t mode) :
			boost::interprocess::shared_memory_object(
					boost::interprocess::open_only, name.c_str(), mode), name(
					name), automaticDelete(name.c_str()) {
	}
private:
	std::string name;
	boost::interprocess::remove_shared_memory_on_destroy automaticDelete;
};

#endif /* IPCSHAREDMEM_H_ */
