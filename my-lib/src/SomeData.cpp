/*
 * SomeData.cpp
 *
 *  Created on: 1 mai 2021
 *      Author: mig
 */

#include "SomeData.hpp"

int SomeData::getWeld1() const {
	return weld1;
}

int SomeData::getWeld2() const {
	return weld2;
}
