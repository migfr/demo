/*
 * IPCMemory.h
 *
 *  Created on: 15 avr. 2021
 *      Author: mig
 */

#ifndef IPCSHAREDDATA_HPP_
#define IPCSHAREDDATA_HPP_

#include <boost/interprocess/creation_tags.hpp>
#include <boost/interprocess/detail/os_file_functions.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/creation_tags.hpp>
#include <string>

#include "IPCSharedMem.hpp"

/**
 * Interprocess shared memory for object of class T.
 * An object of class T will be created.
 *
 * T MUST implement lock() and unlock() so that shared memory
 * is protected by an interprocess mutex such as
 * boost::interprocess::interprocess_mutex
 */
template<class T>
class IPCSharedData {
	const std::string &name;
	T *data;
	IPCSharedMem sharedMem;
	boost::interprocess::mapped_region mappedRegion;

public:
	explicit IPCSharedData(boost::interprocess::open_or_create_t,
			const std::string &_name) :
			name(_name), sharedMem(boost::interprocess::open_or_create, name,
					boost::interprocess::read_write, sizeof(T)), mappedRegion(
					sharedMem, boost::interprocess::read_write) {
		data = (T*) mappedRegion.get_address();
		// Now create a new T at address
		new (data) T;
	}
	explicit IPCSharedData(boost::interprocess::open_only_t,
			const std::string &_name) :
			name(_name), sharedMem(boost::interprocess::open_only, name,
					boost::interprocess::read_write), mappedRegion(sharedMem,
					boost::interprocess::read_write) {
		data = (T*) mappedRegion.get_address();
		// DO NOT create a new T at address, it was created by other process
	}

	void lock() {
		data->lock();
	}
	void unlock() {
		data->unlock();
	}
	T& getData() {
		return *data;
	}
};

#endif /* IPCSHAREDDATA_HPP_ */
