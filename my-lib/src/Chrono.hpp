/*
 * Chrono.h
 *
 *  Created on: 17 avr. 2021
 *      Author: mig
 */

#ifndef CHRONO_HPP_
#define CHRONO_HPP_
#include <chrono>
class Chrono {
	std::chrono::time_point<std::chrono::high_resolution_clock> start =
			std::chrono::high_resolution_clock::now();
public:
	Chrono() = default;
	virtual ~Chrono() = default;
	std::chrono::milliseconds getElapsed() const;
	void reset();
};

#endif /* CHRONO_HPP_ */
