################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Chrono.cpp \
../src/IPCSharedData.cpp \
../src/SomeData.cpp 

OBJS += \
./src/Chrono.o \
./src/IPCSharedData.o \
./src/SomeData.o 

CPP_DEPS += \
./src/Chrono.d \
./src/IPCSharedData.d \
./src/SomeData.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp src/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -I../boost_1_75_0 -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


