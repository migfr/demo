//============================================================================
// Name        : demo.cpp
// Author      : WeezU
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <boost/interprocess/creation_tags.hpp>
#include <Chrono.hpp>
#include <IPCSharedData.hpp>
#include <SomeData.hpp>
#include <chrono>
#include <iostream>

int main(int argc, char **argv) {
	char const *location("mymem");
	IPCSharedData<SomeData> IPCMem(boost::interprocess::open_only, location);

	int w1;
	int w2;
	int last_w1 = -1;
	int last_w2 = -1;
	Chrono timer, global_timer;
	std::chrono::milliseconds get_lock, get_w1, get_w2, get_unlock;

	while (true) {
		timer.reset();
		IPCMem.lock();
		get_lock = timer.getElapsed();
		w1 = IPCMem.getData().getWeld1();
		get_w1 = timer.getElapsed();
		w2 = IPCMem.getData().getWeld2();
		get_w2 = timer.getElapsed();
		IPCMem.unlock();
		get_unlock = timer.getElapsed();
		if (w1 != w2 && (last_w1 != w1 || last_w2 != w2)) {
			std::cerr << "error: got two different values " << w1 << "/" << w2
					<< std::endl;
		}
		if (last_w1 % 25 == 0 && last_w1 != w1) {
			std::cout << "reached " << last_w1 << ", timings "
					<< get_lock.count() << ", " << get_w1.count() << ", "
					<< get_w2.count() << ", " << get_unlock.count()
					<< " elapsed " << global_timer.getElapsed().count()
					<< std::endl;
		}
		last_w1 = w1;
		last_w2 = w2;
	}
	return 0;
}
