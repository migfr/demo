/*
 * IPCSharedData.cpp
 *
 *  Created on: 1 mai 2021
 *      Author: mig
 */

#include <boost/interprocess/creation_tags.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <Chrono.hpp>
#include <gtest/gtest.h>
#include <IPCSharedData.hpp>
#include <SomeData.hpp>
#include <unistd.h>
#include <chrono>
#include <thread>

const int WELD1 = 84;
const int DELTA = 15;
const int WELD2 = WELD1 + DELTA;

/**
 * child process : initiliazes shared data, waits for child to modify value,
 * then checks value
 */
void parent() {
	IPCSharedData<SomeData> shared(boost::interprocess::open_or_create, "bla");
	boost::interprocess::remove_shared_memory_on_destroy automaticDelete("bla");
	shared.getData().lock();
	shared.getData().setWeld1(WELD1);
	shared.getData().setWeld2(WELD2);
	shared.getData().unlock();
	std::this_thread::sleep_for(std::chrono::seconds(2));
	shared.getData().lock();
	ASSERT_EQ(shared.getData().getWeld1(), WELD1 + 1);
	ASSERT_EQ(shared.getData().getWeld2(), WELD2 + 1);
	shared.getData().unlock();
	// Now we will loop for 10 seconds while child updates values, and check for consistency
	// sleep time was chosen to be prime with child cycle...
	Chrono timer;
	while (timer.getElapsed() < std::chrono::seconds(12)) {
		shared.lock();
		ASSERT_EQ(shared.getData().getWeld1() + WELD2 - WELD1,
				shared.getData().getWeld2());
		shared.unlock();
		std::this_thread::sleep_for(std::chrono::milliseconds(47));
	}
}

/**
 * child process : waits to let parent initialize shared data, checks value
 * then modifies value and exits
 */
void child() {
	std::this_thread::sleep_for(std::chrono::seconds(1));
	IPCSharedData<SomeData> shared(boost::interprocess::open_only, "bla");
	shared.getData().lock();
	ASSERT_EQ(shared.getData().getWeld1(), WELD1);
	ASSERT_EQ(shared.getData().getWeld2(), WELD2);
	shared.getData().unlock();

	shared.getData().lock();
	shared.getData().setWeld1(WELD1 + 1);
	shared.getData().setWeld2(WELD2 + 1);
	shared.getData().unlock();

	// Wait for 2 seconds to let parent check above values, then start updating stuff
	std::this_thread::sleep_for(std::chrono::seconds(2));
	Chrono timer;
	while (timer.getElapsed() < std::chrono::seconds(10)) {
		// update values at random, leaving some time in between updates for parent to see them
		int random = std::min(RAND_MAX - DELTA,
				std::min(std::rand(), INT_MAX - DELTA));
		shared.lock();
		shared.getData().setWeld1(random);
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
		shared.getData().setWeld2(random + DELTA);
		shared.unlock();
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
	exit(testing::Test::HasFailure());
}

TEST(IPCSharedMem, fulltest) {
	auto pid = fork();
	if (pid == 0) {
		child();
	} else {
		parent();
	}
}
