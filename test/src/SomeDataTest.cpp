#include <gtest/gtest.h>
#include <SomeData.hpp>
TEST(SomeData, Init) {
	auto x1 = SomeData().getWeld1();
	auto x2 = SomeData().getWeld2();
	ASSERT_EQ(x1, 0);
	ASSERT_EQ(x2, 0);
}
TEST(SomeData, Modify) {
	SomeData x;
	x.setWeld1(2);
	x.setWeld2(3);
	int x1 = x.getWeld1();
	int x2 = x.getWeld2();
	ASSERT_EQ(x1, 2);
	ASSERT_EQ(x2, 3);
}
TEST(InterprocessData, init) {
	InterprocessData x;
	ASSERT_NO_THROW(x.lock());
	ASSERT_NO_THROW(x.unlock());
}
