/*
 * ChronoTest.cpp
 *
 *  Created on: 30 avr. 2021
 *      Author: mig
 */

#include <Chrono.hpp>
#include <gtest/gtest.h>

TEST(Chrono, init) {
	Chrono c;
	ASSERT_NO_THROW(c.getElapsed());
}
TEST(Chrono, reset) {
	Chrono c;
	c.reset();
	ASSERT_NO_THROW(c.getElapsed());
}
TEST(Chrono, get) {
	Chrono c;
	ASSERT_TRUE(c.getElapsed() >= std::chrono::milliseconds(0));
}
