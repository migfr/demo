################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/ChronoTest.cpp \
../src/IPCSharedDataTest.cpp \
../src/SomeDataTest.cpp \
../src/SomeOtherDataTest.cpp 

OBJS += \
./src/ChronoTest.o \
./src/IPCSharedDataTest.o \
./src/SomeDataTest.o \
./src/SomeOtherDataTest.o 

CPP_DEPS += \
./src/ChronoTest.d \
./src/IPCSharedDataTest.d \
./src/SomeDataTest.d \
./src/SomeOtherDataTest.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp src/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -I../../my-lib/include -I../../my-lib/src -I../../my-lib/boost_1_75_0 -O0 -g3 -ftest-coverage -fprofile-arcs -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


