//============================================================================
// Name        : demo.cpp
// Author      : WeezU
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <IPCSharedData.hpp>
#include <SomeData.hpp>
#include <iostream>
#include <thread>
#include <chrono>

int main(int argc, char **argv) {
	char const *location("mymem");
	IPCSharedData<SomeData> IPCMem(boost::interprocess::open_or_create, location);

	while (true) {
		IPCMem.lock();
		IPCMem.getData().setWeld1(IPCMem.getData().getWeld1() + 1);
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
		IPCMem.getData().setWeld2(IPCMem.getData().getWeld2() + 1);
		IPCMem.unlock();
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	return 0;
}
